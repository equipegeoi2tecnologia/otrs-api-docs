# E-DESK API Docs


[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/28f6d04ea8e0134e1542)

### Endpoint
``http://192.168.0.118/otrs/nph-genericinterface.pl/Webservice/GenericTicketConnectorTCE``

*Todos os métodos necessitam de autenticação para acessar-los. Você pode optar por enviar os valores UserLogin e Password, ou passar um SessionID, isso é possível utilizando o método **/Session/Create***

### POST `/Session/Create`

*Cria um SessionID válido para ser utilizado nos outros métodos do WebService*

#### Parâmetros

```
{
    "UserLogin" : "usuario",
    "Password" : "senha"
}
```

#### Resposta
```
{
    "SessionID": "57UawqUBJ0I2tDBY0zIdsCt7czk1Cg81"
}
```


### GET `/Session/Get`

*Busca os dados da SessionID gerada*

#### Parâmetros

```
{
    "SessionID" : "57UawqUBJ0I2tDBY0zIdsCt7czk1Cg81"
}
```

#### Resposta
```
{
    "SessionData": [
        {
            "Value": "25",
            "Key": "AdminCommunicationLogPageShown"
        },
        {
            "Value": "25",
            "Key": "AdminDynamicFieldsOverviewPageShown"
        },
        {
            "Value": "25",
            "Key": "AdminKPIOverviewPageShown"
        },
        {
            "Value": "2018-05-10 20:40:51",
            "Key": "ChangeTime"
        },
        {
            "Value": "2018-05-10 20:40:51",
            "Key": "CreateTime"
        }
        ...
    ]
}
```


### POST `/Ticket/Create`

*Cria um ticket no E-DESK. Um ticket deve conter um **Article** e pode conter vários anexos, todos os campos dinâmicos podem ser utilizados nesta operação.*

#### Parâmetros

```json
{
	"UserLogin": "",
	"Password": "",
	"CustomerUserLogin": "",
	"SessionID": "XpioFcLnOTh5fratBHPHuoD83Ea3U2Y8",
	"Ticket": {
		"Title" : "Primeiro Ticket criado na API com documentação!",
		"QueueID" : "",
		"Queue" : "",
		"TypeID": "",
		"Type": "",
		"ServiceID" : "",
		"Service":"",
		"SLAID": "",
		"SLA": "",
		"StateID": "",
		"PriorityID": "",
		"Priority": "",
		"OwnerID": "",
		"Owner": "",
		"ResponsibleID": "",
		"Responsible": "",
		"CustomerUser": "",
		"CustomerID" : "",
		"PendingTime": {
			"Diff" : "",
			"Year": "",
			"Month": "",
			"Day": "",
			"Hour": "",
			"Minute": ""
		}
	},
	"Article": {
		"CommunicationChannelID": "",
		"CommunicationChannel": "",
		"IsVisibleForCustomer": "",
		"SenderTypeID": "",
		"SenderType": "",
		"From": "",
		"Subject": "",
		"Body": "",
		"ContentType": "",
		"Charset": "",
		"MimeType": "",
		"HistoryType": "",
		"HistoryComment": "",
		"AutoResponseType": "",
		"TimeUnit": "",
		"NoAgentNotify": "",
		"ForceNotificationToUserID": "",
		"ExcludeNotificationToUserID": "",
		"ExcludeMuteNotificationToUserID": ""
	},
	"DynamicField": {
		"Name": "nomedocampo",
		"Value": "valor"
	},
	"Attachment": { 
	    "Content" : "arquivo encodado em base64" 
    	"ContentType": "data:image/jpeg;charset=utf-8;base64",
    	"Filename": "resolucao.jpg"
	}
}
```

#### Resposta
```
{
    "SessionData": [
        {
            "Value": "25",
            "Key": "AdminCommunicationLogPageShown"
        },
        {
            "Value": "25",
            "Key": "AdminDynamicFieldsOverviewPageShown"
        },
        {
            "Value": "25",
            "Key": "AdminKPIOverviewPageShown"
        },
        {
            "Value": "2018-05-10 20:40:51",
            "Key": "ChangeTime"
        },
        {
            "Value": "2018-05-10 20:40:51",
            "Key": "CreateTime"
        }
        ...
    ]
}
```


### POST `/Ticket/Update`

*Atualiza um ticket no E-DESK.*


#### Parâmetros

```json
{
	"UserLogin": "",
	"Password": "",
	"CustomerUserLogin": "",
	"SessionID": "XpioFcLnOTh5fratBHPHuoD83Ea3U2Y8",
	"TicketID" : 45,
	"Ticket": {
		"Title" : "Primeiro Ticket atualizado na API com documentação!",
		"QueueID" : "",
		"Queue" : "",
		"TypeID": "",
		"Type": "",
		"ServiceID" : "",
		"Service":"",
		"SLAID": "",
		"SLA": "",
		"StateID": "",
		"PriorityID": "",
		"Priority": "",
		"OwnerID": "",
		"Owner": "",
		"ResponsibleID": "",
		"Responsible": "",
		"CustomerUser": "",
		"CustomerID" : "",
		"PendingTime": {
			"Diff" : "",
			"Year": "",
			"Month": "",
			"Day": "",
			"Hour": "",
			"Minute": ""
		}
	},
	"Article": {
		"CommunicationChannelID": "",
		"CommunicationChannel": "",
		"IsVisibleForCustomer": "",
		"SenderTypeID": "",
		"SenderType": "",
		"From": "",
		"Subject": "",
		"Body": "",
		"ContentType": "",
		"Charset": "",
		"MimeType": "",
		"HistoryType": "",
		"HistoryComment": "",
		"AutoResponseType": "",
		"TimeUnit": "",
		"NoAgentNotify": "",
		"ForceNotificationToUserID": "",
		"ExcludeNotificationToUserID": "",
		"ExcludeMuteNotificationToUserID": ""
	},
	"Attachment": {
	    "Content" : "arquivo encodado em base64" 
    	"ContentType": "data:image/jpeg;charset=utf-8;base64",
    	"Filename": "resolucao.jpg"
	}
}
```

#### Resposta
```
{
    "Ticket": {
        "Age": 59629,
        "PriorityID": "3",
        "ServiceID": "",
        "Type": "Solicitação de Serviço",
        "Responsible": "apiotrs@imagetech.corp",
        "StateID": "4",
        "ResponsibleID": "11",
        "ChangeBy": "13",
        "EscalationTime": "0",
        "Changed": "2018-05-15 12:12:35",
        "OwnerID": "13",
        "RealTillTimeNotUsed": "0",
        "GroupID": "31",
        "Owner": "apiotrs@imagetech.corp",
        "CustomerID": "apiotrs",
        "TypeID": "4",
        "Created": "2018-05-14 19:38:46",
        "Priority": "3 normal",
        "UntilTime": 0,
        "EscalationUpdateTime": "0",
        "QueueID": "31",
        "Queue": "Nível 3::Nível 3 - e-TCE",
        "State": "Em andamento",
        "Title": "Primeiro Ticket criado na API com documentação!",
        "CreateBy": "13",
        "TicketID": "45",
        "DynamicField": [
            {
                "Value": null,
                "Name": "Complexity"
            },
            {
                "Value": null,
                "Name": "CustomerContract"
            },
            {
                "Value": null,
                "Name": "ITSMCriticality"
            },
            {
                "Value": null,
                "Name": "ITSMDecisionDate"
            },
            ...
        ],
        "StateType": "open",
        "Article": {
            "ContentType": "text/plain; charset=utf8",
            "SenderTypeID": "1",
            "ReplyTo": "",
            "References": "",
            "ContentCharset": "utf8",
            "CreateBy": "13",
            "SenderType": "agent",
            "TicketID": "45",
            "Body": "O ticket foi atualizado mais uma vez para validar a integração",
            "ChangeBy": "13",
            "ChangeTime": "2018-05-15 12:12:35",
            "Cc": "",
            "MimeType": "text/plain",
            "IsVisibleForCustomer": "1",
            "Subject": "Mais uma atualização",
            "InReplyTo": "",
            "CreateTime": "2018-05-15 12:12:35",
            "IncomingTime": "1526386355",
            "Charset": "utf8",
            "CommunicationChannelID": "3",
            "Bcc": "",
            "MessageID": "",
            "ArticleNumber": 5,
            "ArticleID": "99",
            "To": "",
            "From": "Diego Pettengill Fernandes"
        },
        "EscalationResponseTime": "0",
        "UnlockTimeout": "1526386355",
        "EscalationSolutionTime": "0",
        "LockID": "1",
        "ArchiveFlag": "n",
        "TicketNumber": "1800035",
        "Lock": "unlock",
        "SLAID": "",
        "CustomerUserID": "apiotrsl@imagetech.corp"
    },
    "ArticleID": "99",
    "TicketNumber": "1800035",
    "TicketID": 45
}
```


### GET `/Ticket/Search`

*Busca os tickets pelos seus atributos, seja pela sua fila, titulo, corpo ou descrição. Retorna uma lista de de TicketIDs baseada na busca.*

#### Parâmetros

```
{
	"SessionID": "XpioFcLnOTh5fratBHPHuoD83Ea3U2Y8", //Obrigatorio
	"Limit" : 100,
	"TicketNumber" : "",
	"Title" : "",
	"Queues" : "",
	"QueueIDs": "27" ,
	"UseSubQueues" : true,
	"Types" : "",
	"TypeIDs": "",
	"States":"",
	"StateIDs":"",
	"StateType":"",
	"StateTypeIDs":"",
	"Priorities":"",
	"PriorityIDs":"",
	"Services":"",
	"ServiceIDs" :"",
	"SLAs":"",
	"SLAIDs":"",
	"Locks":"",
	"LockIDs":"",
	"OwnerIDs":"",
	"ResponsibleIDs":"",
	"WatchUserIDs":"",
	"CustomerID":"",
	"CustomerUserLogin":"",
	"CreatedUserIDs":"",
	"CreatedTypes":"",
	"CreatedTypeIDs":"",
	"CreatedPriorities":"",
	"CreatedPriorityIDs":"",
	"CreatedStates":"",
	"CreatedStateIDs":"",
	"CreatedQueueIDs": "",
	"DynamicField": {
	    "Name":"",
	    "Equals":"",
	    "Like":"",
	    "GreaterThan":"",
	    "GreaterThanEquals":"",
	    "SmallerThan":"",
	    "SmallerThanEquals":""
	},
	"Ticketflag":{
	    "Seen" :""
	},
	"From" : "",
	"To" : "",
	"Cc" : "",
	"Subject":"",
	"Body":"",
	"FullTextIndex":"",
	"ContentSearch":"",
	"ConditionInline":"",
	"ArticleCreateTimeOlderMinutes":"",
	"ArticleCreateTimeNewerMinutes":"",
	"ArticleCreateTimeNewerDate":"",
	"ArticleCreateTimeOlderDate":"",
	"TicketCreateTimeOlderMinutes":"",
	"ATicketCreateTimeNewerMinutes":"",
	"TicketCreateTimeNewerDate":"",
	"TicketCreateTimeOlderDate":"",
	"TicketLastChangeTimeOlderMinutes":"",
	"TicketLastChangeTimeNewerMinutes":"",
	"TicketLastChangeTimeNewerDate":"",
	"TicketLastChangeTimeOlderDate":"",
	"TicketChangeTimeOlderMinutes":"",
	"TicketChangeTimeNewerMinutes":"",
	"TicketChangeTimeNewerDate":"",
	"TicketChangeTimeOlderDate":"",
	"TicketCloseTimeOlderMinutes":"",
	"TicketCloseTimeNewerMinutes":"",
	"TicketCloseTimeNewerDate":"",
	"TicketCloseTimeOlderDate":"",
	"TicketPendingTimeOlderMinutes":"",
	"TicketPendingTimeNewerMinutes":"",
	"TicketPendingTimeNewerDate":"",
	"TicketPendingTimeOlderDate":"",
	"TicketEscalationTimeOlderMinutes":"",
	"TTicketEscalationTimeNewerMinutes":"",
	"TicketEscalationTimeNewerDate":"",
	"TicketEscalationTimeOlderDate":"",
	"ArchiveFlags": "",
	"OrderBy":"",
	"SortBy":"",
	"CustomerUserID":""
	
}
```

#### Resposta
```
{
    "TicketID": [
        "46",
        "45",
        "44",
        "43",
        "42",
        "41",
        "40",
        "39",
        "38",
        "37",
        "36",
        "35",
        "34",
        "33",
        "32",
        "31",
        "30",
        ...
    ]
}
```


### GET `/TicketHistory/Get`

*Retorna o historico do ticket.*

#### Parâmetros

```
{
	"SessionID": "XpioFcLnOTh5fratBHPHuoD83Ea3U2Y8",
	"TicketID": "45"
}
```

#### Resposta
```
{
    "TicketHistory": [
        {
            "History": [
                {
                    "PriorityID": "3",
                    "CreateTime": "2018-05-14 19:38:46",
                    "OwnerID": "1",
                    "QueueID": "3",
                    "HistoryType": "NewTicket",
                    "TypeID": "4",
                    "HistoryTypeID": "1",
                    "ArticleID": 0,
                    "StateID": "1",
                    "CreateBy": "13",
                    "TicketID": "45",
                    "Name": "%%1800035%%Lixeira%%3 normal%%Novo%%45"
                },
                {
                    "PriorityID": "3",
                    "CreateTime": "2018-05-14 19:38:46",
                    "OwnerID": "1",
                    "QueueID": "3",
                    "HistoryType": "ServiceUpdate",
                    "TypeID": "4",
                    "HistoryTypeID": "38",
                    "ArticleID": 0,
                    "StateID": "1",
                    "CreateBy": "13",
                    "TicketID": "45",
                    "Name": "%%NULL%%%%NULL%%"
                },
                {
                    "PriorityID": "3",
                    "CreateTime": "2018-05-14 19:38:46",
                    "OwnerID": "1",
                    "QueueID": "3",
                    "HistoryType": "SLAUpdate",
                    "TypeID": "4",
                    "HistoryTypeID": "39",
                    "ArticleID": 0,
                    "StateID": "1",
                    "CreateBy": "13",
                    "TicketID": "45",
                    "Name": "%%NULL%%%%NULL%%"
                },
                {
                    "PriorityID": "3",
                    "CreateTime": "2018-05-14 19:38:46",
                    "OwnerID": "1",
                    "QueueID": "3",
                    "HistoryType": "TypeUpdate",
                    "TypeID": "4",
                    "HistoryTypeID": "37",
                    "ArticleID": 0,
                    "StateID": "1",
                    "CreateBy": "13",
                    "TicketID": "45",
                    "Name": "%%Solicitação de Serviço%%4"
                },
                {
                    "PriorityID": "3",
                    "CreateTime": "2018-05-14 19:38:46",
                    "OwnerID": "1",
                    "QueueID": "3",
                    "HistoryType": "CustomerUpdate",
                    "TypeID": "4",
                    "HistoryTypeID": "21",
                    "ArticleID": 0,
                    "StateID": "1",
                    "CreateBy": "13",
                    "TicketID": "45",
                    "Name": "%%CustomerID=diego.pettengill;CustomerUser=diego.pettengill@imagetech.corp;"
                },
                {
                    "PriorityID": "3",
                    "CreateTime": "2018-05-14 19:38:46",
                    "OwnerID": "1",
                    "QueueID": "3",
                    "HistoryType": "NewTicket",
                    "TypeID": "4",
                    "HistoryTypeID": "1",
                    "ArticleID": "93",
                    "StateID": "1",
                    "CreateBy": "13",
                    "TicketID": "45",
                    "Name": "%%GenericInterface Create"
                },
                {
                    "PriorityID": "3",
                    "CreateTime": "2018-05-14 19:38:46",
                    "OwnerID": "1",
                    "QueueID": "3",
                    "HistoryType": "Misc",
                    "TypeID": "4",
                    "HistoryTypeID": "25",
                    "ArticleID": 0,
                    "StateID": "1",
                    "CreateBy": "13",
                    "TicketID": "45",
                    "Name": "Reset of unlock time."
                },
                {
                    "PriorityID": "3",
                    "CreateTime": "2018-05-14 19:38:46",
                    "OwnerID": "13",
                    "QueueID": "3",
                    "HistoryType": "OwnerUpdate",
                    "TypeID": "4",
                    "HistoryTypeID": "23",
                    "ArticleID": 0,
                    "StateID": "1",
                    "CreateBy": "13",
                    "TicketID": "45",
                    "Name": "%%diego.pettengill@imagetech.corp%%13"
                },
                {
                    "PriorityID": "3",
                    "CreateTime": "2018-05-14 19:38:46",
                    "OwnerID": "13",
                    "QueueID": "3",
                    "HistoryType": "Lock",
                    "TypeID": "4",
                    "HistoryTypeID": "17",
                    "ArticleID": 0,
                    "StateID": "1",
                    "CreateBy": "13",
                    "TicketID": "45",
                    "Name": "%%lock"
                },
                {
                    "PriorityID": "3",
                    "CreateTime": "2018-05-14 19:38:46",
                    "OwnerID": "13",
                    "QueueID": "3",
                    "HistoryType": "ResponsibleUpdate",
                    "TypeID": "4",
                    "HistoryTypeID": "34",
                    "ArticleID": 0,
                    "StateID": "1",
                    "CreateBy": "13",
                    "TicketID": "45",
                    "Name": "%%gustavo@imagetech.corp%%11"
                },
                {
                    "PriorityID": "3",
                    "CreateTime": "2018-05-14 19:38:46",
                    "OwnerID": "13",
                    "QueueID": "3",
                    "HistoryType": "SendCustomerNotification",
                    "TypeID": "4",
                    "HistoryTypeID": "10",
                    "ArticleID": "94",
                    "StateID": "1",
                    "CreateBy": "13",
                    "TicketID": "45",
                    "Name": "%%diego@geoi2.com.br"
                },
                {
                    "PriorityID": "3",
                    "CreateTime": "2018-05-14 19:58:26",
                    "OwnerID": "13",
                    "QueueID": "3",
                    "HistoryType": "AddNote",
                    "TypeID": "4",
                    "HistoryTypeID": "15",
                    "ArticleID": "95",
                    "StateID": "1",
                    "CreateBy": "13",
                    "TicketID": "45",
                    "Name": "%%GenericInterface Note"
                },
                {
                    "PriorityID": "3",
                    "CreateTime": "2018-05-14 19:58:26",
                    "OwnerID": "13",
                    "QueueID": "3",
                    "HistoryType": "Misc",
                    "TypeID": "4",
                    "HistoryTypeID": "25",
                    "ArticleID": 0,
                    "StateID": "1",
                    "CreateBy": "13",
                    "TicketID": "45",
                    "Name": "Reset of unlock time."
                },
                {
                    "PriorityID": "3",
                    "CreateTime": "2018-05-14 20:03:10",
                    "OwnerID": "13",
                    "QueueID": "31",
                    "HistoryType": "Move",
                    "TypeID": "4",
                    "HistoryTypeID": "16",
                    "ArticleID": 0,
                    "StateID": "1",
                    "CreateBy": "13",
                    "TicketID": "45",
                    "Name": "%%Nível 3::Nível 3 - e-TCE%%31%%Lixeira%%3"
                },
                {
                    "PriorityID": "3",
                    "CreateTime": "2018-05-14 20:03:10",
                    "OwnerID": "13",
                    "QueueID": "31",
                    "HistoryType": "Unlock",
                    "TypeID": "4",
                    "HistoryTypeID": "18",
                    "ArticleID": 0,
                    "StateID": "1",
                    "CreateBy": "1",
                    "TicketID": "45",
                    "Name": "%%unlock"
                },
                {
                    "PriorityID": "3",
                    "CreateTime": "2018-05-14 20:03:10",
                    "OwnerID": "13",
                    "QueueID": "31",
                    "HistoryType": "StateUpdate",
                    "TypeID": "4",
                    "HistoryTypeID": "27",
                    "ArticleID": 0,
                    "StateID": "4",
                    "CreateBy": "13",
                    "TicketID": "45",
                    "Name": "%%Novo%%Em andamento%%"
                },
                {
                    "PriorityID": "3",
                    "CreateTime": "2018-05-14 20:03:10",
                    "OwnerID": "13",
                    "QueueID": "31",
                    "HistoryType": "AddNote",
                    "TypeID": "4",
                    "HistoryTypeID": "15",
                    "ArticleID": "96",
                    "StateID": "4",
                    "CreateBy": "13",
                    "TicketID": "45",
                    "Name": "%%GenericInterface Note"
                },
                {
                    "PriorityID": "3",
                    "CreateTime": "2018-05-14 20:03:10",
                    "OwnerID": "13",
                    "QueueID": "31",
                    "HistoryType": "Misc",
                    "TypeID": "4",
                    "HistoryTypeID": "25",
                    "ArticleID": 0,
                    "StateID": "4",
                    "CreateBy": "13",
                    "TicketID": "45",
                    "Name": "Reset of unlock time."
                },
                {
                    "PriorityID": "3",
                    "CreateTime": "2018-05-15 12:12:35",
                    "OwnerID": "13",
                    "QueueID": "31",
                    "HistoryType": "AddNote",
                    "TypeID": "4",
                    "HistoryTypeID": "15",
                    "ArticleID": "99",
                    "StateID": "4",
                    "CreateBy": "13",
                    "TicketID": "45",
                    "Name": "%%GenericInterface Note"
                },
                {
                    "PriorityID": "3",
                    "CreateTime": "2018-05-15 12:12:35",
                    "OwnerID": "13",
                    "QueueID": "31",
                    "HistoryType": "Misc",
                    "TypeID": "4",
                    "HistoryTypeID": "25",
                    "ArticleID": 0,
                    "StateID": "4",
                    "CreateBy": "13",
                    "TicketID": "45",
                    "Name": "Reset of unlock time."
                }
            ],
            "TicketID": "45"
        }
    ]
}
```